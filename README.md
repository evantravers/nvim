# nvim

Install and configure nvim

## Role Variables


```yaml
nvim:
  ftplugins:
    - file_type: ruby
      config: |
        setlocal tabstop=2
        setlocal shiftwidth=2
        setlocal softtabstop=2
        setlocal expandtab
    - file_type: go
      config: |
        setlocal tabstop=4
        setlocal shiftwidth=4
        setlocal softtabstop=4
        setlocal noexpandtab

  config:
    - name: Use relative line numbers
      config: set relativenumber

  plugins:
    - name: itspriddle/vim-marked
      opts: "'for': 'markdown,vimwiki'"
      config: |
        nnoremap <Leader>M :MarkedOpen<CR>
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - faen.nvim

## License

MIT
